package ch.verttigo.craftok.coinsbooster.Listener;

import ch.verttigo.craftok.coinsbooster.CoinsBooster;
import ch.verttigo.craftok.coinsbooster.Utils.Utils;
import com.andrei1058.bedwars.api.events.gameplay.GameEndEvent;
import com.andrei1058.bedwars.api.events.player.PlayerBedBreakEvent;
import com.andrei1058.bedwars.api.events.player.PlayerKillEvent;
import com.andrei1058.bedwars.api.events.upgrades.UpgradeBuyEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;
import java.util.UUID;

public class BedWarsEvent implements Listener {

    @EventHandler
    public void bedBreakEvent(PlayerBedBreakEvent e) {
        try {
            Player p = e.getPlayer();
            if (Utils.inGame(p)) {
                final double money = (CoinsBooster.booster + 1) * CoinsBooster.getInstance().getConfig().getDouble("baseCoinKillBedWars");
                CoinsBooster.getEconomy().depositPlayer(p, money);
                p.sendMessage(CoinsBooster.getInstance().getConfig().getString("playerRewards")
                        .replace("%Coins%", String.valueOf(money))
                        .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone broke a bed, but he's null... WTF");
        }
    }

    @EventHandler
    public void playerKillEvent(PlayerKillEvent e) {
        try {

            Player p = e.getKiller();
            if (Utils.inGame(p)) {
                final double money = (CoinsBooster.booster + 1) * CoinsBooster.getInstance().getConfig().getDouble("baseCoinKillBedWars");
                CoinsBooster.getEconomy().depositPlayer(p, money);
                p.sendMessage(CoinsBooster.getInstance().getConfig().getString("playerRewards")
                        .replace("%Coins%", String.valueOf(money))
                        .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone killed someone , but he's null... WTF");
        }

    }


    @EventHandler
    public void playerLevelUP(UpgradeBuyEvent e) {
        try {
            Player p = e.getPlayer();
            if (Utils.inGame(p)) {
                final double money = (CoinsBooster.booster + 1) * CoinsBooster.getInstance().getConfig().getDouble("baseCoinLevelUpBedWars");
                CoinsBooster.getEconomy().depositPlayer(p, money);
                p.sendMessage(CoinsBooster.getInstance().getConfig().getString("playerRewards")
                        .replace("%Coins%", String.valueOf(money))
                        .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone levelup, but he's null... WTF");
        }

    }


    @EventHandler
    public void gameEndEvent(GameEndEvent e) {
        try{
            List loosers = e.getLosers();
            List winners = e.getWinners();
            if (Utils.inGame(Bukkit.getOfflinePlayer((UUID) winners.get(0)).getPlayer())) {
                loosers.forEach((uuid) -> {
                    Player p = Bukkit.getPlayer((UUID) uuid).getPlayer();
                    double money = CoinsBooster.getInstance().getConfig().getDouble("baseCoinLooseBedwars");
                    money = (CoinsBooster.booster + 1) * money;
                    CoinsBooster.getEconomy().depositPlayer(p, money);
                    p.sendMessage(CoinsBooster.getInstance().getConfig().getString("bonusEndGame")
                            .replace("%Coins%", String.valueOf(money))
                            .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
                });
                winners.forEach((uuid) -> {
                    Player p = Bukkit.getPlayer((UUID) uuid).getPlayer();
                    double money = CoinsBooster.getInstance().getConfig().getDouble("baseCoinWinBedwars");
                    money = (CoinsBooster.booster + 1) * money;
                    CoinsBooster.getEconomy().depositPlayer(p, money);
                    p.sendMessage(CoinsBooster.getInstance().getConfig().getString("bonusEndGame")
                            .replace("%Coins%", String.valueOf(money))
                            .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
                });
            }
        }catch(NullPointerException error) {
            System.out.println("CoinsBooster : Someone end the game, but he's null... WTF");
        }

    }

}
