package ch.verttigo.craftok.coinsbooster.Listener;


import ch.verttigo.craftok.coinsbooster.CoinsBooster;
import ch.verttigo.craftok.coinsbooster.Utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class JoinQuitEvent implements Listener {


    @EventHandler
    public void onPlayerJoin(org.bukkit.event.player.PlayerJoinEvent e) {
        try {
            Player p = e.getPlayer();
            if (Utils.isVanished(p)) return;
            if (Utils.inLobby(p)) {
                CoinsBooster.booster = Utils.currentBooster();
                if (CoinsBooster.booster > 3) CoinsBooster.booster = 3;
                if (Utils.haveBoost(p)) {
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        if (CoinsBooster.booster == 3) {
                            all.sendMessage(CoinsBooster.getInstance().getConfig().getString("boosterMax")
                                    .replace("%Player%", p.getName())
                                    .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
                        } else {
                            all.sendMessage(CoinsBooster.getInstance().getConfig().getString("playerJoin")
                                    .replace("%Player%", p.getName())
                                    .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
                        }
                    }
                }
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone join, but he's null... WTF");
        }
    }

    @EventHandler
    public void onPlayerQuit(org.bukkit.event.player.PlayerQuitEvent e) {
        try {
            Player p = e.getPlayer();
            if (Utils.isVanished(p)) return;
            if (Utils.inLobby(p)) {
                CoinsBooster.booster = Utils.currentBooster();
                if (Utils.haveBoost(p)) {
                    if (CoinsBooster.booster < 3) {
                        for (Player all : Bukkit.getOnlinePlayers()) {
                            all.sendMessage(CoinsBooster.getInstance().getConfig().getString("playerQuit")
                                    .replace("%Player%", p.getName())
                                    .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
                        }
                    }
                }
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone quit, but he's null... WTF");
        }

    }
}
