package ch.verttigo.craftok.coinsbooster.Listener;

import ch.verttigo.craftok.coinsbooster.CoinsBooster;
import ch.verttigo.craftok.coinsbooster.Utils.Utils;
import com.syntaxphoenix.jumpleagueplusapi.events.CheckPointEvent;
import com.syntaxphoenix.jumpleagueplusapi.events.DeathEvent;
import com.syntaxphoenix.jumpleagueplusapi.events.GameEndEvent;
import com.syntaxphoenix.jumpleagueplusapi.events.GameStartEvent;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;

public class JLEvent implements Listener {

    public static ArrayList<Player> players = new ArrayList<>();

    @EventHandler
    public void onDeath(DeathEvent e) {
        try {
            Player p = e.getKiller();
            if (Utils.inGame(p)) {
                p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
                final double money = (CoinsBooster.booster + 1) * CoinsBooster.getInstance().getConfig().getDouble("onKillPlayer");
                CoinsBooster.getEconomy().depositPlayer(p, money);
                p.sendMessage(CoinsBooster.getInstance().getConfig().getString("playerRewards")
                        .replace("%Coins%", String.valueOf(money))
                        .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone killed someone , but he's null... WTF");
        }
    }


    @EventHandler
    public void onReachCheckpoint(CheckPointEvent e) {
        try {
            Player p = e.getPlayer();
            if (Utils.inGame(p)) {
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
                final double money = ((CoinsBooster.booster + 1) * CoinsBooster.getInstance().getConfig().getDouble("onReachCheckpoint")) + (e.getCheckpoint() - 1);
                CoinsBooster.getEconomy().depositPlayer(p, money);
                p.sendMessage(CoinsBooster.getInstance().getConfig().getString("playerRewards")
                        .replace("%Coins%", String.valueOf(money))
                        .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone reached CK , but he's null... WTF");
        }
    }

    @EventHandler
    public void onGameStart(GameStartEvent e) {
        players.addAll(Bukkit.getOnlinePlayers());
    }

    @EventHandler
    public void onGameEnd(GameEndEvent e) {
        try {
            Player p = e.getWinner();
            if (Utils.inGame(p)) {
                double money = CoinsBooster.getInstance().getConfig().getDouble("onGameEndWinner");
                money = (CoinsBooster.booster + 1) * money;
                CoinsBooster.getEconomy().depositPlayer(p, money);
                p.sendMessage(CoinsBooster.getInstance().getConfig().getString("bonusEndGame")
                        .replace("%Coins%", String.valueOf(money))
                        .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));

                players.forEach((pa) -> {
                    if (pa == p) return;
                    double money2 = CoinsBooster.getInstance().getConfig().getDouble("onGameEndLooser");
                    money2 = (CoinsBooster.booster + 1) * money2;
                    CoinsBooster.getEconomy().depositPlayer(pa, money2);
                    pa.sendMessage(CoinsBooster.getInstance().getConfig().getString("bonusEndGame")
                            .replace("%Coins%", String.valueOf(money2))
                            .replace("%CurrentBooster%", String.valueOf(CoinsBooster.booster + 1)));
                });
            }
        } catch (NullPointerException error) {
            System.out.println("CoinsBooster : Someone end the game, but he's null... WTF");
        }
    }

}
