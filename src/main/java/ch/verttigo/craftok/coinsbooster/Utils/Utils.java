package ch.verttigo.craftok.coinsbooster.Utils;

import ch.verttigo.craftok.coinsbooster.CoinsBooster;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.BridgeServiceProperty;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

public class Utils {

    public static final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);

    public static boolean inGame(Player p) {
        ServiceInfoSnapshot server = CloudNetDriver.getInstance().getCloudServiceProvider().
                getCloudService(playerManager.getOnlinePlayer(p.getUniqueId()).getConnectedService().getUniqueId());
        return server.getProperty(BridgeServiceProperty.MOTD).get().equalsIgnoreCase("ingame");
    }

    public static boolean inLobby(Player p) {
        ServiceInfoSnapshot server = CloudNetDriver.getInstance().getCloudServiceProvider().
                getCloudService(playerManager.getOnlinePlayer(p.getUniqueId()).getConnectedService().getUniqueId());
        return server.getProperty(BridgeServiceProperty.MOTD).get().equalsIgnoreCase("waiting");
    }

    public static boolean isVanished(Player player) {
        for (MetadataValue meta : player.getMetadata("vanished")) {
            if (meta.asBoolean()) return true;
        }
        return false;
    }

    public static boolean haveBoost(Player p) {
        if (p.hasPermission("coinsbooster.minivip") || p.hasPermission("coinsbooster.vip")
                || p.hasPermission("coinsbooster.vipplus") || p.hasPermission("coinsbooster.legend")) {
            return true;
        } else {
            return false;
        }
    }

    public static double currentBooster() {
        double booster = 0;
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.hasPermission("coinsbooster.minivip"))
                booster += CoinsBooster.getInstance().getConfig().getDouble("boosterMini");
            if (p.hasPermission("coinsbooster.vip"))
                booster += CoinsBooster.getInstance().getConfig().getDouble("boosterVIP");
            if (p.hasPermission("coinsbooster.vipplus"))
                booster += CoinsBooster.getInstance().getConfig().getDouble("boosterVIPPLUS");
            if (p.hasPermission("coinsbooster.legend"))
                booster += CoinsBooster.getInstance().getConfig().getDouble("boosterLEGEND");

        }
        return booster;
    }

}
