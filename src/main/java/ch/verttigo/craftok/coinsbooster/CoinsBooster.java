package ch.verttigo.craftok.coinsbooster;

import ch.verttigo.craftok.coinsbooster.Listener.BedWarsEvent;
import ch.verttigo.craftok.coinsbooster.Listener.JLEvent;
import ch.verttigo.craftok.coinsbooster.Listener.JoinQuitEvent;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class CoinsBooster extends JavaPlugin {

    public static boolean plFinded = false;
    public static double booster = 0;
    public static CoinsBooster plugin;
    private static Economy econ = null;

    @Override
    public void onEnable() {
        plugin = this;
        plugin.saveDefaultConfig();
        this.getServer().getPluginManager().registerEvents(new JoinQuitEvent(), this);

        if (!setupEconomy()) {
            getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (Bukkit.getPluginManager().isPluginEnabled("BedWars1058")) {
            getLogger().info("BedWars founded");
            plFinded = true;
            this.getServer().getPluginManager().registerEvents(new BedWarsEvent(), this);
            return;
        }
        if (Bukkit.getPluginManager().isPluginEnabled("JumpLeaguePlusAPI") && Bukkit.getPluginManager().isPluginEnabled("JumpLeaguePlus")) {
            getLogger().info("Jump League founded");
            plFinded = true;
            this.getServer().getPluginManager().registerEvents(new JLEvent(), this);
            return;
        }
        if (!plFinded) {
            getLogger().info("[%s] - Disabled due to no games plugins found!");
            getServer().getPluginManager().disablePlugin(this);
        }
    }


    public static CoinsBooster getInstance() {
        return plugin;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }
}
